# Global Microbial Gene Catalog (GMGC) data

This is the data for the publication:

> Coelho, L.P., Alves, R., del Río, Á.R. et al. Towards the biogeography of
> prokaryotic genes. Nature 601, 252–256 (2022).
> https://doi.org/10.1038/s41586-021-04233-4

Please cite this publication if you use these results.

This repository contains all the publicly available data for the Global
Microbial Gene Catalog (GMGC), version 1.0 as a git-annex repository.

The files are described in more detail in the [online
documentation](https://gmgc.embl.de/download.cgi). Feel free to contact us [on
the mailing-list](https://groups.google.com/forum/#!forum/gmgc-users) with
questions.

**Note**: downloading the full data takes 1.7 Terabytes of data and *is highly
redundant*. We recommend that you only download those files that you will
actually use. For example, the files `GMGC10.95nr.fna` and
`subcatalogs/GMGC10.95nr.fna.gz` contain the same data in different formats:
the compressed version saves space, but the uncompressed version can be used
with the `GMGC10.95nr.fna.dhi`
[index](https://github.com/BigDataBiology/fasta_reader) for fast access to
specific sequences. Similarly, all the subcatalogs are subsets of the main one,
optimized for different uses and tradeoffs, and can be built from the main
catalog and the information in the `metadata/GMGC10.gene-environment.tsv` file.


## Usage

### Install git-annex

You need to install [git-annex](https://git-annex.branchable.com/). For
example, if you have conda available, you can generally get it using the
following:

```bash
conda create -n gmgc.data
conda install -c conda-forge git-annex
```

### Getting data

If `git-annex` is installed, retrieving data can be done with `git-annex get`.
For example:

```bash
git-annex get GMGC10.95nr.faa GMGC10.95nr.faa.dhi
```

This will download the data and check for consistency (using SHA256 hashing).

The file `URLS.txt` also contains all the URLs from which the data can be
downloaded.

